<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="estilos/estilos2.css">
     <title>informacion</title>

</head>
<body>
<header>
          <div class="menu">
               <nav>
                    <ul>
                         <li><a href="info.php">Home</a></li>
                         <li><a href="formulario.php">Registrar Alumnos</a></li>
                         <li><a href="cerrarSesion.php">Cerrar Sesión</a></li>
                    </ul>
               </nav>
          </div>
          
     </header> 

<form action="" method="post">
     <label for="num_cuenta" class="form-label">Número de Cuenta</label>
     <input name="num_cuenta" type="text" class="form-registro-input" id="input-num_cuenta-registro" placeholder="Escriba su Numero de Cuenta">
          
     <label for="nombre" class="form-label">Nombre</label>
     <input name="nombre" type="text" class="form-registro-input" id="input-nombre-registro" placeholder="Escriba su Nombre">
     <label for="primer_apellido" class="form-label">Primer Apellido</label>
          <input name="primer_apellido" type="text" class="form-registro-input" id="input-primer_apellido-registro" placeholder="Escriba su Primer Apellido">

          <label for="segundo_apellido" class="form-label">Segundo Apellido</label>
          <input name="segundo_apellido" type="text" class="form-registro-input" id="input-segundo_apellido-registro" placeholder="Escriba su Segundo Apellido">

         <div class="radio-genero"> 
              <label for="genero" class="form-label">Género</label>
              <label for="hombre" class="form-genero">Hombre</label>
              <input name="genero" type="radio" class="form-genero" id="input-genero-registro-hombre">
              <label for="mujer" class="form-genero">Mujer</label>
              <input name="genero" type="radio" class="form-genero" id="input-genero-registro-mujer">
              <label for="otro" class="form-genero">Otro</label>
              <input name="genero" type="radio" class="form-genero" id="input-genero-registro-otro">
         </div>

          <label for="fecha_nac" class="form-label">Fecha de Nacimiento</label>
          <input name="fecha_nac" type="date" class="form-registro-input" id="input-fecha_nac-registro">

     <label for="contrasenia" class="form-label">Contrasenia</label>
     <input name="contrasenia" type="password" class="form-registro-input" id="input-contrasenia-registro" placeholder="Escriba su Contrasenia">
     
     <br>
     <input class="button" type="submit" value="Registrar" name="btnAgregar">

</form>

<?php
if(isset($_POST["btnAgregar"])){
     $num_cuenta =$_POST['num_cuenta'];
     $nombre=$_POST['nombre'];
     $primerApellido=$_POST['primer_apellido'];
     $segundoApellido=$_POST['segundo_apellido'];
     $genero=$_POST['genero'];
     $fechaNac=$_POST['fecha_nac'];
     $contrasenia=$_POST['contrasenia'];


     $_SESSION['Alumno'] = [
          1=>[
               'num_cuenta' => $num_cuenta,
               'nombre' => $nombre,
               'primer_apellido' => $primerApellido,
               'segundo_apellido' => $segundoApellido,
               'contrasenia' => $contrasenia,
               'genero' => $genero,
               'fecha_nac'=> $fechaNac
          ],
     ];
     echo "<script>alert('Cuenta $num_cuenta ha sido agregado');</script>";
     //echo "<a href='info.php'>Regresar</a>";
     header("location:info.php");

}

?>
     
</body>
</html>

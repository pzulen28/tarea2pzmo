<?php
session_start();

if(!empty($_SESSION['Alumno'])){
     foreach($_SESSION['Alumno'] as $alumno){
          echo "<p>Hola:</p>".$alumno['nombre'];
     }
}    

?>
 
<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="estilos/estilos.css">
     <title>Home</title>

</head>
     
<body>


<header>
          <div class="menu">
               <nav>
                    <ul>
                         <li><a href="#">Home</a></li>
                         <li><a href="formulario.php">Registro de Alumnos</a></li>
                         <li><a href="cerrarSesion.php">Cerrar Sesión</a></li>
                    </ul>
               </nav>
          </div>
          
     </header> 
    <main>
    <section id="section-table-autenticacion">
     
     <div class="div-table-autUsuario">
          <h2>Usuario Autenticado</h2>
          <table class="tabla-autUsuarios">
               <thead class = thead-autUsuario>
                    <tr><th>Admin General</th></tr>
               </thead>
               <?php foreach($_SESSION['Alumno'] as $alumno => $value):?>
               <tr><td class="informacion"><h2> Información</h2>
               <br>
               <?php echo"Numero de cuenta: " .$value['num_cuenta']?><br><br>

               <?php echo"Fecha de Nacimiento: ".$value['fecha_nac']?>
               </td></tr>
               <?php endforeach ?>
          </table>

     </div>
     </section>

     <section id="section-table-datos-guardados">
          <div class="div-table-datGuardados">
               <h1>Datos guardados</h1>
               <table class="tabla-datGuardados">
               
                    <thead>                    
                         <tr class="sin-borde">
                              <th>#</th>
                              <th>Nombre</th>
                              <th>Fecha de Nacimiento</th>
                         </tr>
                    </thead>
                    <?php foreach($_SESSION['Alumno'] as $alumno => $value):?>
                    <tr>
                         <td class="datos columna"><?php echo $value['num_cuenta'] ?></td>
                         <td class="datos"><?php echo $value['nombre'] ?></td>
                         <td class="datos"><?php echo $value['fecha_nac'] ?></td>
                    </tr>
                    <?php endforeach ?>
               </table>
          </div>
     </section>
    </main>
     
</body>
</html>